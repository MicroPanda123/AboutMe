---
title: "Top 10 best encryption software (2024)!!!"
description: My personal recommendation list of my favourite encryption software
pubDate: 2024.12.01
editDate: 2024.12.01
tags: ["FDE", "encryption", "cryptography", "software", "Full Disk Encryption", "E2EE"]
---

[Go back](/blog)

hi guys in today's blog post we will cover the top 10 encryption software in 2024 please remember to like subscribe and comment your suggestions below  

Okay, but in all seriousness, I intend for this to be a short guide about what tools I personally recommend for specific encryption needs. It's definitely not exhaustive.  

<details>
<summary>Table of Contents</summary>

- [Communication](#communication)
  - [Instant Messaging](#instant-messaging)
  - [Discord alternatives](#discord-alternatives)
  - [Email](#email)
- [Encrypting files](#encrypting-files)
  - [Public-key encryption](#public-key-encryption)
  - [Private-key encryption](#private-key-encryption)
  - [File sharing](#file-sharing)
  - [File syncing](#file-syncing)
  - [Backups](#backups)
- [Full Disk Encryption](#full-disk-encryption)
  - [Linux](#linux)
  - [Windows](#windows)
  - [MacOS](#macos)
- [Libraries](#libraries)

</details>

## Communication  

The encrypted communication landscape is wide and confusing, so here are my general suggestions.  

### Instant Messaging  

[Signal](https://signal.org/). If you really want something self-hosted, maybe look into [Matrix](https://matrix.org/), but you will have to deal with its flaws (For exa-Unable to decrypt message) or dubious decisions (Like blocking messages with Telegram links on unencrypted chats). [SimpleX](https://simplex.chat) also looks promising, but it's not yet exactly user-friendly, especially when it comes to using it on multiple devices.  

### Discord alternatives 

This is a tough one, but [Matrix](https://matrix.org/) could also be used for that. For Voice Chat, you can use [Mumble](https://www.mumble.com/). There are other options, however this specific blog post is about encrypted solutions.  

### Email  

Stop using email for safe communication\[[1](https://news.ycombinator.com/item?id=16088386)\]. No, seriously! Email is broken and leaks tons of metadata about you. If you really have to, then PGP (which is [pretty bad](https://www.latacora.com/blog/2019/07/16/the-pgp-problem/)) is currently your only option if you want something integrated with most email clients, unless you want to encrypt using external software (which I'll cover later).  

However, if you want to communicate safely, really, don't use email for that.

## Encrypting files  

### Public-key encryption  

[Age](https://age-encryption.org/) is a great public-key encryption solution. It's meant as a simple way to encrypt data for a given public key. It can generate small keys, which you can send over a message to someone, or you can alternatively use your SSH key for encrypting and decrypting. Simple, good, better than PGP.

### Private-key encryption  

If you want to encrypt singular files: you can use Age as well! Just generate yourself a new key and use it, or you can use password encryption, which Age supports as well. Honorable mention will be: [Picocrypt](https://github.com/Picocrypt/Picocrypt), it's good GUI/CLI tool that also allows simple password encryption, it's in honorable mentions because it is no longer maintained, but it still is a great tool, which was audited by Radical Open Security.

  

For encrypting folders, it depends on your use case: if you simply want to store multiple files, I personally would use tar + Age/Picocrypt, 7zip is also a good option as well since it supports ASE256 encryption too. If you want to encrypt files going into a cloud storage, like google cloud, and you still want to use sync functionality, I recommend 2 options: [Cryptomator](https://cryptomator.org/) and [Rclone](https://rclone.org/).  

Cryptomator is more intuitive for casual users?, but I'd recommend looking into Rclone if you aren't scared of CLI applications, it has "crypt" remote which can be used for encryption of other remotes.  

### File sharing  

For encrypted file sharing I recommend 3 options: [croc](https://github.com/schollz/croc), [magic-wormhole](https://github.com/magic-wormhole/magic-wormhole) and [OnionShare](https://onionshare.org/). All of those allow transferring files between computers. First two work in pretty similar way, you get a code to share and other person types it in. [OnionShare](https://onionshare.org/) allows to send files via TOR network, hosting file temporarely on an Onion link, which adds additional privacy to the transfer (however you can also use two other options via TOR using it as a proxy).   

Alternatively, you can also just encrypt file(s) using previous methods and sent it to one of the file hosting services, like gofile.  

### File syncing  

For secure and encrypted file syncing, I highly recommend [Syncthing](https://syncthing.net/). I am a huge fan of it, because it seems to work like magic. Files are encrypted in transit using TLS and it pretty much just works.

### Backups

For backups I recomment using [restic](https://restic.net), it's fast, efficient and of course backups are encrypted.  

## Full Disk Encryption  

This section is mostly about FDE of OS disk, however most of these tools can also be used to encrypt external drives. Keep in mind that, despite my high recommendation, FDE can lead to irreversible data loss in case of corruption or losing a password (remember about backups!).  

### Linux

Here, the choice is pretty straightforward:

LUKS2 - Probably most used FDE standard on Linux. Basic usage is:

    # Encrypt a device
    cryptsetup luksFormat /dev/{device path}
    
    # Unlock a device
    cryptsetup open /dev/{path} {mapped device name}
    
    # Mount a mapped device
    mount /dev/mapper/{mapped device name} {mountpoint}

FDE for an OS partition/drive is a little more involved, but most distro's should be able to handle it itself.

### Windows

Windows Pro editions come with its own built-in FDE software: bitlocker. It's enabled by default in recent versions of the OS, however it has its issues:

*   It's proprietary
*   Not available for Home editions

I personally would recommend disabling it (if it's turned on by default) and using [Veracrypt](https://veracrypt.fr), setup is relatively easy: after installation you select System -> Encrypt System Partition/Drive and you follow the steps there. Veracrypt is open source and battle tested, and that's why I recommend it.

### MacOS

I have not used macOS, so I can't really say much here, but like Windows macOS comes with its own built-in FDE: FileVault, it's used automatically on Apple Silicon and since no other options exist (as far as I'm aware, if you know something about it please message me!) I recommend using it, in my personal opinion it's still better than nothing.  

## Libraries   

Lots of cryptography libraries exist, here's a very short, non-exhaustive list of common ones for some popular languages:  

General (has lots of bindings in other languages) : [libsodium](https://doc.libsodium.org/)  

Rust: [rust-crypto](https://github.com/DaGenix/rust-crypto) (this is a bit of self-promotion because I'm a small contributor there :3)  

Python: [cryptography](https://github.com/pyca/cryptography)  

Go: [crypto](https://pkg.go.dev/crypto)  

JavaScript: [Web Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/Crypto)  

You generally should research what library you should choose, but these are the ones I've used.  

  

## Summary

So that's the whole list for now, it might be updated with new stuff, but that's all for now!  
Thank you for reading, I appreciate it :)

[Go back](/blog)
