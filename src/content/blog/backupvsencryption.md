---
title: "Backups: how to undermine your encryption."
description: Recently, when reviewing my NAS setup, I realized that it contains a critical vulnerability that undermines the entire encryption implementation. I shall tell you this (short) story now.
pubDate: 2024.05.25
editDate: 2024.05.25
tags: ["Encryption", "Backups", "Tech"]
---

[Go back](/blog)

**edit**: It seems I have not explained my setup clearly enough, which could lead (and already did) to misunderstanding. My setup has 4 drives in total: 3 drives are in encrypted RAID 5 array, those are the drives I am talking about in this post. The drive that contains Restic configuration file is on the 4th drive, it is a boot/system drive for Raspberry Pi, which for reasons can not be currently made encrypted, this is why keys inside config files were dangerous. Now with that out of the way, have a nice read :)

Recently, when reviewing my NAS setup, I realized that it contains a critical
vulnerability that undermines the entire encryption implementation. I shall tell
you this (short) story now.

When you want to keep important files secure, you usually do two things:
1. Keep them encrypted.
2. Keep them backed up.

The first step is to keep them secure, and the second step is to keep them safe.

I indeed had both. I am using RAID 5 with an encrypted LUKS 2 (aes-xts-plain64
with argon2i) partition. To enhance security, I decided not to store the
encryption key anywhere. So, when the NAS reboots, I log in and manually unlock
the partition. This way, it is probably the most secure since the only option
left for an attacker would be to brute force the key. Given that the key is long
and randomly generated, this is practically impossible.

However, the problem was with the way I implemented backups.

For backups, I use Restic. In my opinion, it's one of the best tools for both
small and large data backups. It's efficient and secure because it also employs
encryption. Because of this, you can use any storage hosting you want, as the
data is encrypted anyway.

Since I wanted to automate the backup process, I stored the repository (what
Restic calls the place where backups are stored) configuration file on the NAS
server.

You can probably see now where the issue lies.

Yes, in the config file there was... a key. The encryption key for Restic
backups. It was stored in plain text on the NAS server. Since the config file
also contained every credential necessary to access the repository, the drive
encryption was entirely undermined (Hey! That's the title!), since data was accessible from backups.


### CONCLUSION

That's all from this short story. You might be curious about how I fixed this,
and the answer is encryption yet again. Now, on the encrypted partition, I keep
encrypted (using (r)age) credentials for backups, and on the NAS server itself,
I keep the key required to decrypt said credentials. Both the encrypted
credentials and the decryption key are backed up in a secure location that I
will not disclose.

Now, since the keys are on the encrypted partition itself, while also being
encrypted, I declare my setup to be officially secure-ish. There are, of course,
potential issues, but for now, this seems fine, since it all would require
physical access to the server either way.

The key (haha, like key from earlier) lesson I want You (and me) to get from this story is: check your backups. But not only for the obvious reason.
Also check your system as a whole too.

Thank You for reading! Bye :)

[Go back](/blog)
