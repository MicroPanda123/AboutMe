---
title: "NixOS on ByteHosting (aka simple networking cheat sheet)"
description: Recently I've installed NixOS on ByteHosting and dealing with networking was pain in the butt, so I decided to make a short writeup about it
pubDate: 2024.10.22
editDate: 2024.10.23
tags: ["VPS", "NixOS", "nix", "networking", "ByteHosting"]
---

[Go back](/blog)

Recently, I've discovered an extremely cool hosting provider, called ByteHosting
(not sponsored), offering very cheap VPS's, however I'm not going to focus on
that.

They offer a small range of preconfigured OS's: Debian, Ubuntu, RHEL-Like
distros and even Windows Server or Proxmox, what they lack is NixOS.

The installation itself is pretty simple and straight-forward, what I had most issue
with was setting up networking during and post installation, so this will be
more like a small cheat sheet for networking commands.

After you buy a VPS on ByteHosting, you must select which OS you want. It
doesn't matter for us, however, if you pick Proxmox, you will not get an option
to mount custom ISOs, so I'd recommend just picking Debian.

Next go into custom ISOs section and add NixOS minimal ISO, paste link from their website, after that you click "mount ISO", restart your
VPS and enter noVNC console.

After the NixOS installer boots, ByteHosting (and most other VPS providers) will not set up network
connection automatically, so you will have to run these commands:

```
ip addr add {IP address provided to you}/{prefix length, usually 24} dev {network interface}
ip route add default via {gateway IP}
echo "nameserver {preferred DNS IP}" > /etc/resolv.con
```

After entering those, you should now have working internet connection in your
installer.

Now, you need to also set up static IP for your VPS, I use this snippet (I'm
aware systemd.network is usually better, but for some reason it did not work for
me):
```
networking = {
   useDHCP = false;
   interfaces.{interface} = {
     ipv4.addresses = [{
       address = {IPv4 address provided to you};
       prefixLength = {usually 24};
     }];
     ipv6.addresses = [{
       address = {IPv6 address provided to you};
       prefixLength = {prefix length, duh};
     }];
   };
   defaultGateway = {
     address = {IPv4 gateway address provided to you};
     interface = {interface};
   };
   defaultGateway6 = {
     address = {IPv6 gateway address provided to you};
     interface = {interface};
   };
   nameservers = [ {list of preferred DNS IPv4/6 addresses} ];
 };
```
After all this, you should have a working NixOS networking setup!

Thanks for reading :) This was pretty short and basic, because I wanted to write
about something I've done recently just to make myself come back to writing.


[Go back](/blog)
