---
title: Let's Encrypt vs ZeroSSL reviewed
description: ZeroSSL's comparison to Let's Encrypt in my opinion is unfair. Let's explore why.
pubDate: 2024.05.21
editDate: 2024.06.18
tags: ["Let's Encrypt", "ZeroSSL", "SSL"]
---

[Go back](/blog)

ZeroSSL in my opinion has no right to compare itself to Let's Encrypt.

I will compare their "ZeroSSL vs Let's Encrypt" page with factual information about this claim, which are accessible on sites of both ZeroSSL and Let's Encrypt

1. 90-Day Certificates - Both ZeroSSL and Let's Encrypt offer free 90-Day certificates and ZeroSSL says it on their page - what they don't say, however, is that ZeroSSL gives you... 3 certificates for free. Yes, there is a limit of 3 certificates per user. That is the main issue I have - a lot of functionality they provide and compare with Let's Encrypt is paid.

2. 1-Year Certificates - Let's Encrypt does not provide those, that's true... however, ZeroSSL does not provide it for free either. It's a paid feature.

3. Multi-Domain Certificates - Both ZeroSSL and Let's Encrypt offer Multi-Domain Certificates, what ZeroSSL does not say on their comparison page is that while Let's Encrypt provides this for free, ZeroSSL does not.

4. Wildcard Certificates - Same as Multi-Domain - both provide them, only one for free (guess who).

5. Email reminders - Both ZeroSSL and Let's Encrypt can send you email reminders that your domain certificates are about to expire. No issue here.

6. Intuitive User Interface - I am not sure what exactly they mean by this but apparently both support it - since I never used (or knew it had) Let's Encrypt user interface, I'll let this slide since ZeroSSL does have a nice User Interface.

7. Account Credentials - ZeroSSL has them, Let's Encrypt not (edit: wrong, see note at the bottom.), is it important? I don't know, but perhaps there are certain usages for it. (I feel like they're probably security related, feel free to message me why is that important)

8. ACME Support - Both providers have it, nothing wrong here.

9. SSL Monitoring - ZeroSSL provides it, Let's Encrypt not... however they do not say what they mean by that - if it's about expiration then emails provide that, if they mean monitoring through API then that's not a free feature (API is paid) - but I'll give it to them, sure, Let's Encrypt does not (by itself) provide SSL monitoring except email expiration notices.

10. REST API - Again, ZeroSSL offers and Let's Encrypt doesn't, but that is yet another feature that is paid in ZeroSSL, so that comparison again is just faulty. (Edit: Also not important because ACME exists.)

11. Domain Verification via Email - ZeroSSL supports it, Let's Encrypt does not. This is fair.

12. Domain Verification via CNAME/File Upload - Both support that, no issue here.

13. Technical Support - This point is probably the reason I started writing this rant. Let's Encrypt is a non-profit that does not require money from you. ZeroSSL is a for-profit company and they provide technical support for paying customers. The free tier does not have technical support. And while I do not expect them to give away technical support for free, I find it unfair to compare that to a non-profit organisation which gives away product for free.

14. Custom Solutions - Same as the previous point, paid only thing - totally fair thing to do, but it does not give them bragging rights here.

15. Partner Program - I don't know what that is, but I assume similar thing to Technical Support and Custom Solutions. However, since I don't know what that means, I'll give them benefit of the doubt. (I doubt it's free, though)

And for the ACME part:

1. Unlimited Certificates - This suggests that like with Let's Encrypt you get, well, unlimited certificates. That is basically not true, at least in the free tier. What you get is 3 certificates with an indefinite ability to renew them. You can at most get certificates for 3 domains at once per account (you can invalidate others and then get new ones, but that does not feel unlimited at all). At Let's Encrypt you can just get certificates for every domain you want (there are certain rules but they are not as restrictive as 3 domains for an account) - sure, there are rate limits but I'll get to that later.
2. Free of Charge - This is technically true, they provide free of charge service.

3. No Rate Limit - Technically true, this is the point where "I'll get to that". So, this creates an image that Let's Encrypt is very restrictive with their rate limiting, which is not true. Let's Encrypt's main limit called "Certificates per Registered Domain" is 50 per week, which means for a given domain you can request 50 certificates per week, and in each certificate you can include up to 100 hostnames, so in a week you can request 50000 hostnames per domain (technically, most likely some limitations apply). Renewals are a bit more restrictive - you can request duplicate certificates 5 times a week. Considering that you do not need to renew certificates that often (after all, you have 90 days), that is not really a blocking limit and if you are at the point where you really need more, then you definitely should use commercial certificates instead of Let's Encrypt ones (and for that - sure, use ZeroSSL if you want). ZeroSSL does not have a rate-limit (as of their claim), however it has a hard limit of 3 certificates.

4. 90-Day Certificates - Refer to point 1 in the previous section.

5. Multi-Domain Certificates - Refer to point 3 in the previous section.

6. Wildcard Certificates - Refer to point 4 in the previous section.

7. Manage Certificates in UI - That's true, Let's Encrypt does not have UI to manage certificates, ZeroSSL does. This one totally fair.

That's it, that's the whole "ZeroSSL vs Let's Encrypt" post written by ZeroSSL.

I don't have a problem with them being a for-profit company that offers some free certificates, I think that's great! Having alternatives is truly a great thing. What I dislike is how they compare themselves to Let's Encrypt, especially parts where they compare the features that they provide for money.

ZeroSSL is a great option if you need the functionality they provide (and you want to give them money) - they provide a lot of things that typical certificate vendors do not provide for a paying customer (not to mention they usually sell you the certificates for a lot more) and I find that great.

But for free certificates, I'd stay with Let's Encrypt.

(If you are here then, yay, thank you for reading to the very end! This is my first longer piece of writing and I am pretty proud of how it turned out, I plan on making more blog posts which are not targetted expose posts in the near future and I hope You'll read it as well! Thank you very much.)

Small edit: Small notes I got from peeps on Fedi: 
- REST API is not really a important feature when ACME is a thing that exists.
- Let's Encrypt actually does have Account Credentials that work via ACME, So point 7 is wrong.

[Go back](/blog)
