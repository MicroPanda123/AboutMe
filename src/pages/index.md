---
layout: ../layouts/IndexLayout.astro
title: MarkAssPandi :D
my_img:
  path: /pfp.webp
  desc:
    link: 'https://twitter.com/razorbun13'
    text: 'Art by @razorbun13'
name: '✨🌈MicroPanda123🌈✨'
---

# Hi, it's nice to meet you!

I am MicroPanda123, also known as MarkAssPandi.

I am a teenage Polish geek, with interest in:
- Cybersecurity
- Privacy
- Random bullshit
- [Anime](/anime)
- [TV shows and movies](/shows) [(Must watch list)](/tldr)
- [Blogging](/blog)

# Projects

- [zasa](https://codeberg.org/MicroPanda123/zasa) - No external dependencies JSON parser in Rust
- [~~gejuchowo.pl~~](https://gejuchowo.pl) - Self-hosted fediverse instance (Rest in Peace [*])
- [renam](https://codeberg.org/MicroPanda123/renam) - Blazingly Fast and simple utility to rename files in a directory using regex
- [rsdisk](https://codeberg.org/MicroPanda123/rsdisk) - fdisk but written in rust and it's also a library
- [simple ass duplication (sad-dd)](https://codeberg.org/MicroPanda123/simple-ass-duplication) - dd-like utility written in rust, that kinda diverged from dd itself.
- [SEWBC](https://codeberg.org/MicroPanda123/SEWBC) - Simple E2EE Web Based Chat, as title suggests, it's a chat web app that has encryption built in.
- [yapm](https://codeberg.org/MicroPanda123/yapm) - Yet Another Package Manager, still in creation, not ready for anything, will either be container based or AUR like.
- [HexEditor](https://codeberg.org/MicroPanda123/hexeditor) - WIP HexEditor, not yet ready for anything lol for now dead
- [Matrix Push Server](https://codeberg.org/MicroPanda123/matrix-push-server) - Proof Of Concept matrix server that pushes requests to a given server (mainly ntfy) when you receive a message
- [OkkuBot](https://codeberg.org/MicroPanda123/OkkuBot) - Discord bot using Andrej Karpathy's [nanoGPT](https://github.com/karpathy/nanoGPT)
- [cdadl_rs](https://codeberg.org/SquirrelPanda/cdadl_rs) - Rewrite of cdadl in Rust using Tauri (dead for now, might return later)
- [cdadl](https://codeberg.org/MicroPanda123/cdadl) - App allowing you to download videos from cda.pl
- [eeg](https://codeberg.org/MicroPanda123/eeg) - Easy Encryption in Go, application to easily encrypt and decrypt files
- [markov chains for text in python implementation](https://codeberg.org/MicroPanda123/markov-chains-for-text-in-python-implementation) - Name is pretty self-explaining, it's Markov Chain for text in Python
- [matixbot](https://codeberg.org/MicroPanda123/matixbot) - Discord bot using markov chain
- [flask UFW OTP](https://codeberg.org/MicroPanda123/FlaskUFWOTP) - Simple web server for managing UFW with OTP
- [league of legends players counter](https://codeberg.org/MicroPanda123/league-of-legends-players-counter) - Discord bot that detects when someone plays League of Legends and makes leaderboard of how long people are playing (I told you, Random bullshit)
- You can see more projects on codeberg and github

# Accounts
- My Forgejo Instance - [git.markasspandi.pl](https://git.markasspandi.pl) (Recently got issues with hosting it, check Codeberg)
- Fediverse - <a rel="me" href="https://fedi.fabrykajabo.li/@MarkAssPandi">MarkAssPandi(at)fabrykajabo.li</a>
- Codeberg - [MicroPanda123](https://codeberg.org/MicroPanda123) (most projects are mirrored there, so if links in [Projects](#Projects) don't work most of them you can find here)
- git.gay - [MicroPanda123](https://git.gay/MicroPanda123) (Mostly mirrors from codeberg)
- Github (I don't rly use it anymore) - [MicroPanda123](https://github.com/MicroPanda123)

# Contact
~~I only (at least for now) accept contact from strangers on email: me@markasspandi.pl or markasspandi@firemail.cc [(please use PGP)](/MarkAssPandi.asc) [(E16AE68C82FE8405DAFFF027BABC909968C1B8EF)](https://keys.openpgp.org/vks/v1/by-fingerprint/E16AE68C82FE8405DAFFF027BABC909968C1B8EF)~~ Email dead for now

You can also find me on Signal: MarkAssPandi.69 , but I don't promise that I will respond.

You can try reach out on stuff from Accounts, but I don't promise that I will respond.

# Credits
- [JustRafau](https://codeberg.org/JustRafau/astro-about-me-template-2) - Making whole astro template for me ❤️
- [@razorbun13](https://twitter.com/razorbun13) - Drew photo for me
