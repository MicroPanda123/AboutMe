---
draft: true
layout: ../layouts/IndexLayout.astro
title: index.md example
my_img:
  path: /favicon.svg
  desc: 
    link: ''
    displayed_text: ''
name: '@your_username'
---
# How to use `index.md`

`draft: true`  
means that it won't be included in the build, but only on the dev server

` layout: ../layouts/IndexLayout.astro `  
selects a layout that this page will use (do not touch please)

`title: a cool title`  
the title, that's gonna be shown on the tab

`my_img:               `  
`  path: favicon.svg   ` - path po image  
`  desc:               `  
`    link: ''          ` - href  
`    displayed_text: ''` - shown text  
neither href nor text filled -> not displayed
only href -> displayed text will be the same
only text -> not clickable
both -> link with text


image shown on the very top the page  
`/file.png` is equal to
```
/
└── public/
    └── file.png  
```  
and `/path/to/file.png` is equal to   
```
/
└── public/
    └── path/
        └── to/
            └── file.png
```

`name: '@your_username'`  
your username (quotation marks are necessary because of `@`).
