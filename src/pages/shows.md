---
layout: ../layouts/IndexLayoutNoPhoto.astro
title: Shows and movies!!!
---

# Favourite* movies and shows 
<sup><sup>* Doesn't have to be favourite, minimum is enjoyed</sup></sup>

## Shows
- **Good Place** - My absolute favourite TV show, brings tears every time
- **Bojack Horseman** - Favourite animated TV show, wish I could watch it for the first time again
- **Money heist** - It was stretched hard, but it was pretty good (but what the hell is up with that english name)
- **Atypical** - Very good
- **Black Mirror** - 6th season slightly fell off, but other than that amazing 
- **Inside Job** - Rick and Morty without the cringe, rly bad it was canceled
- **Modern Family** - Funny comedy
- **The Umbrella Academy** - Very good, last season was eh but waiting for next one to fix it. *edit*: They did, last season is great.
- **Superstore** - Another funny comedy, I enjoy sitcoms
- **The Bing Bang Theory** - Yes I watched it as well, 3 times, Yes I enjoyed it 
- **The Office** - Read above (skip 3 times)
- **Young Sheldon** - Don't @ me
- **Stranger Things** - Rly good first season, rest was eh but still enjoyable
- **Egzorcysta** - Polish comedy about exorcist, funny
- **Rick and Morty** - I know what I said, still was enjoyable for some time

## Movies
- **Wall-e** - My absolute favourite animated movie
- **Up** - Second one
- **The Lego Batman Movie** - Amazing
- **Heathers** - Not a movie, musical, but still amazing had to include it 
- **Spider-man: Into the Spider-verse** - Close to first 3, I really enjoyed it, very good music soundtrack 
- **Spider-Man: Across the Spider-Verse** - Very good, like first one, but ending got me dissatisfied
- **How the Grinch Stole Christmas (2000)** - Made me cry hard, was reletable
- **Shiny_Flakes: The Teenage Drug Lord** - My fav documentary, cus drugs and computer :p
- **John Wick series** - Simple yet rly good movies (Imo 3 was kinda mid tho)
- **Ace Ventura series** - Ignoring slight transphobia, good comedies
- **Ted** - Dirty but funny

## Musicals
- **Heathers** - First musical I watched, absolutely amazing
- **Dear Evan Hansen** - Second musical, movie was mid, live act version is amazing
- **The Greatest Showman** - Only watched movie, incredible.

[Go back](/)
