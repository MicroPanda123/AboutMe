---
layout: ../layouts/IndexLayoutNoPhoto.astro
title: Animeeee
---

# My Anime List (Not to be mistaken with MyAnimeList)

## Watched (From most favourite to least) (List is not full, I forgot like 50% of stuff I watched)
- **Angel Beats** - 10/10 - My absolute favourite, I recommend to everyone
- **Saiki Kusuo no Ψ-nan** - 10/10 - My favourite comedy, also highly recommended
- **Bocchi The Rock!** - 9.9/10 - Aaaaaa love it love it absolutely, tho ending was anticlimatic
- **Puella Magi Madoka Magica** - 9.5/10 - Rly good, only watched first series tho 
- **One Punch Man (Season 1)** - 9/10 - Very good as well
- **Yuri!!! on Ice** - 9/10 - Good, enjoyed, little too cryptic about their relationship
- **Toradora** - 9/10 - Enjoyed greatly, rly cliché at times tho 
- **BNA** - 9/10 - Rly good 
- **Blend S.** - 8/10 - Enjoyed
- **Komi Can't Communicate** - 8/10 - Enjoyable, but 2nd season felt like slightly different show
- **Romantic Killer** - 7/10 - Nice to watch
- **Blue Period** - 6/10 - Just, good.
- **Sword Art Online** - 5/10 - Nghnghnhnh, did make me emotional at times tho

## To Be Watched
For now IDK, I'm open for suggestions tho ;)

[Go back](/)
