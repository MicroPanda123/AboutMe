---
layout: ../layouts/IndexLayoutNoPhoto.astro
title: Must Watch !!
---

# TLDR Must Watch

- Angel Beats ([Anime](/anime))
- Good Place ([Show](/shows))
- Bojack Horseman ([Show](/shows))
- The Lego Batman Movie ([Movie](/shows))
- Spider-man: Into the Spider-verse ([Movie](/shows))

[Go back](/)