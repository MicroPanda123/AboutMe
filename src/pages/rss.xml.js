import rss from '@astrojs/rss';
import { getCollection } from 'astro:content';

export const get = async () => {
  const posts = await getCollection("blog");
  return rss({
    title: 'MarkAssPandi blog!!!',
    description: 'MarkAssPandi\'s blog',
    site: import.meta.env.SITE,
    items: posts.map((post) => ({
      title: post.data.title,
      pubDate: post.data.pubDate,
      description: post.data.description,
      link: `/blog/${post.slug}`,
    })),
    customData: `<language>en-gb</language`,
  })
}
