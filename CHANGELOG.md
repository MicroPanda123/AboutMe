# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

## [Unreleased]

## [1.1.0] - 2024-12-01
- Changed domain
- Sorting blog posts desc
- Shows date next to blog posts

## [1.0.1] - 2023-06-21

- added image description on about-me (index) page


## [1.0.0] - 2023-06-09

### Added

- index (about me page)
- metadata control (sort of)
